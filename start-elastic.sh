#! /bin/bash
docker-compose up --no-start
docker-compose start elasticsearch
echo -n 'waiting for Elasticsearch'
until [[ $(curl -s 'http://localhost:9200/_cluster/health?wait_for_status=green&timeout=120s') ]]; do 
  echo -n '.'; 
  sleep 1; 
done
docker-compose start kibana
echo -n 'waiting for Kibana'
until [[ $(curl -s 'http://localhost:5601/app/kibana') != '' ]] && [[ $(curl -s 'http://localhost:5601/app/kibana') != 'Kibana server is not ready yet' ]]; do
  echo -n '.';
  sleep 1;
done
docker-compose run --rm heartbeat /usr/share/heartbeat/heartbeat setup --index-management --template --ilm-policy
docker-compose start heartbeat
echo 'Cluster is Up'