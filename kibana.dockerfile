ARG tag
FROM docker.elastic.co/kibana/kibana:${tag}
COPY kibana.yml /usr/share/kibana/config/kibana.yml
USER root
RUN chown root:kibana /usr/share/kibana/config/kibana.yml
USER kibana