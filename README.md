# Kibana Canvas Dashboard

Icon colors for Kibana Darkmode

```css
.canvasPage {
  --warning: #FFCE7A;
  --good: #7DE2D1;
  --error: #F66;
  --status-background: #121212;
}
```

Example Expression

```coffee
csv 
"cell,label,status,message
good-example,Good,--good,
warning-example,Warning,--warning,This is a warning
error-example,Error,--error,This is an error"
| markdown 
  "
## 
{{#each rows}}
`` `{{cell}}
{{label}}
`` `
{{/each}}
" 
  font={font family="Arial, sans-serif" size=24 align="center" color="#BBBBBB" weight="normal" underline=false italic=false}
| render 
  css="
.canvasMarkdown {
  display: flex;
  flex-flow: row wrap;
  justify-content: space-around;
  align-content: flex-start;
  align-items: flex-start;
  background-color: var(--status-background);
  border-radius: 1em;
  height: 100%;
  overflow: visible;
}
.canvasMarkdown > * {
  flex: 0 0 auto;
}
.canvasMarkdown hr {
  width: 95%;
  height: 0px;
}
.canvasMarkdown h2 {
  width: 100%;
  overflow: hidden;
  white-space: nowrap;
}
.canvasMarkdown h2::after {
  content: 'Example Box';
}
.canvasMarkdown pre {
  display: contents;
  font-family: inherit;
  font-size: inherit;
}
.canvasMarkdown pre code {
  display: table-cell;
  color: black;
  background-color: grey;
  font-family: inherit;
  font-size: inherit;
  padding-left: 1em;
  padding-right: 1em;
}
.canvasMarkdown .language-good-example {
  background-color: var(--good);
}
.canvasMarkdown .language-warning-example {
  background-color: var(--warning);
}
.canvasMarkdown .language-error-example {
  background-color: var(--error);
}
"
```