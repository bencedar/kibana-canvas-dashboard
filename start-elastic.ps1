$ErrorActionPreference = "SilentlyContinue"
docker-compose build
docker-compose up --no-start
docker-compose start elasticsearch
Write-Host -NoNewline "Waiting for Elasticsearch"
do {
    Start-Sleep -Seconds 1
    Write-Host -NoNewline '.'
    $elasticResult = Invoke-WebRequest http://localhost:9200/_cluster/health?wait_for_status=yellow
} until ($elasticResult.StatusCode -eq 200)
Write-Host "Elasticsearch is Up"
docker-compose start kibana
Write-Host -NoNewline "Waiting for Kibana"
do {
    Start-Sleep -Seconds 1
    Write-Host -NoNewline '.'
    $kibanaResult = Invoke-WebRequest http://localhost:5601/app/kibana
} until (($kibanaResult.StatusCode -eq 200) -and ($kibanaResult.RawContent -ne 'Kibana Server is not ready yet'))
Write-Host "Kibana is up"
docker-compose run --rm heartbeat /usr/share/heartbeat/heartbeat setup --index-management --template --ilm-policy
docker-compose start heartbeat
Write-Host "Cluster is Up"